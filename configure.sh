#!/bin/sh

for i in "sed" "rm" "cp" "mkdir" "gcc" "nasm" "ld" "mformat" "mcopy" "qemu"
do
    echo -n "Checking $i: "
    if ! which $i 2> /dev/null; then 
	echo "...Not Ok"
	exit 1
    fi
    
done

if [ ! -f ~/.mtoolsrc ]; then
    echo "creating a mtools configuration files : ~/.mtoolsrc"
    cat <<EOF > ~/.mtoolsrc
# # file floppy image
 drive a: file="/tmp/floppy.image"
EOF
else
    echo "You already have a mtools configuration files : ~/.mtoolsrc"
    echo "Please move this files and run this script again."
    exit 1
fi
    

echo "Done with the configuration!"
echo "run make or BUILD=elf make"
