#!/bin/sh

DSK=$1
DSKF=$2
echo "mformat -C -f 2880 $DSK"
mformat -C -f 2880 $DSK
echo "mcopy src/kernel.bin $DSK"
mcopy src/kernel.bin $DSK
echo "mv $DSKF src/fdb_kernel.img"
mv $DSKF fdb_kernel.img
