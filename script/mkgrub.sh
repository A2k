#!/bin/sh
STAGE1=/usr/lib/grub/i386-pc/stage1
STAGE2=/usr/lib/grub/i386-pc/stage2
FLOPPY=/var/tmp/floppy.image
rm /tmp/floppy.image
mformat -f 1440 -C a:

mmd a:/boot a:/boot/grub

mcopy $STAGE1 a:/boot/grub/
mcopy $STAGE2 a:/boot/grub/
cat <<EOF > menu.lst
title Alma kernel (kernel.bin) on fd1
root (fd1)
kernel (fd1)/kernel.bin
boot
EOF
mcopy menu.lst a:/boot/grub/menu.lst
rm -f menu.lst
mdir a:/boot/grub a:
