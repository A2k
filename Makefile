DSK_NAME = "a:"
DSK_IMG = "/tmp/floppy.image"

RM = rm -f

main:
	make qemu

fdb_kernel.img:
	make -C src
	$(SHELL) script/mkfloppy.sh  $(DSK_NAME) $(DSK_IMG)

clean:
	make -C src clean
	make -C dummy clean
	make -C crtl clean
	$(RM) fdb_kernel.img

qemu:
	make fdb_kernel.img
	qemu -fda grub_img/fda_grub.img -fdb fdb_kernel.img -boot a
