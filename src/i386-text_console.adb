pragma No_Run_Time;
package body I386.Text_Console is

   Current_Line : console_Index := 1;
   Current_Column : Console_Index := 1;

   procedure Put (C : Character)
   is
      Text_Video : Text_Buffer;
      for Text_Video'Address use To_Address (Unsigned_32 (Video_Base_Address));
      L : Line_Index := Max_Line;
   begin
      if Current_Column > Max_Column then
         Current_Column := 1;
         Current_Line := Current_Line + 1;
      end if;
      if Current_Line > Max_Line then
         for I in 1 .. Max_Line - 1 loop
--          for J in 1 .. Max_Column loop
--             Text_Video (I)(J) := Text_Video (I + 1)(J);
--          end loop;
            Text_Video (I) := Text_Video (I + 1);
         end loop;
--           for I in 1 .. Max_Column loop
--              Text_Video (Max_Line)(I).Text := Character'Pos (Ascii.NUL);
--           end loop;
         Text_Video (Max_Line) := (others => Blank_Text_Element);
         Current_Line := 25;
      end if;
      Text_Video (Current_Line)(Current_Column).Text := Character'Pos (C);
      Current_Column := Current_Column + 1;
   end Put;

   procedure Put (S : String)
   is
   begin
      for I in S'Range loop
         Put (S (I));
      end loop;
   end Put;

   procedure Cls
   is
      Text_Video : Text_Buffer;
      for Text_Video'Address use To_Address (Unsigned_32 (Video_Base_Address));
   begin
--        for I in Line_Index'Range loop
--           for J in Column_Index'Range loop
--              Text_Video (I)(J).Text := Character'Pos (Ascii.NUL);
--           end loop;
--        end loop;
      Text_Video := (others => (others => Blank_Text_Element));
      Current_Line := 1;
      Current_Column := 1;
   end Cls;

   procedure New_Line
   is
   begin
      Current_Line := Current_Line + 1;
      Current_Column := 1;
   end New_Line;

   procedure Put_Line (S : String)
   is
   begin
      Put (S);
      New_Line;
   end Put_Line;

end I386.Text_Console;
