------------------------------------------------------------------------------
--                                                                          --
--                         GNAT RUN-TIME COMPONENTS                         --
--                                                                          --
--                       A D A . E X C E P T I O N S                        --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
--          Copyright (C) 1992-2005, Free Software Foundation, Inc.         --
--                                                                          --
-- This specification is derived from the Ada Reference Manual for use with --
-- GNAT. The copyright notice above, and the license provisions that follow --
-- apply solely to the  contents of the part following the private keyword. --
--                                                                          --
-- GNAT is free software;  you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 2,  or (at your option) any later ver- --
-- sion.  GNAT is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License --
-- for  more details.  You should have  received  a copy of the GNU General --
-- Public License  distributed with GNAT;  see file COPYING.  If not, write --
-- to  the  Free Software Foundation,  51  Franklin  Street,  Fifth  Floor, --
-- Boston, MA 02110-1301, USA.                                              --
--                                                                          --
-- As a special exception,  if other files  instantiate  generics from this --
-- unit, or you link  this unit with other files  to produce an executable, --
-- this  unit  does not  by itself cause  the resulting  executable  to  be --
-- covered  by the  GNU  General  Public  License.  This exception does not --
-- however invalidate  any other reasons why  the executable file  might be --
-- covered by the  GNU Public License.                                      --
--                                                                          --
-- GNAT was originally developed  by the GNAT team at  New York University. --
-- Extensive contributions were provided by Ada Core Technologies Inc.      --
--                                                                          --
------------------------------------------------------------------------------

--  This version is used for all Ada 2005 builds. It differs from a-except.ads
--  only with respect to the addition of Wide_[Wide]Exception_Name functions.
--  The additional entities are marked with pragma Ada_05, so this extended
--  unit is also perfectly suitable for use in Ada 95 or Ada 83 mode.

--  The reason for this splitting off of a separate version is that bootstrap
--  compilers often will be used that do not support Ada 2005 features, and
--  Ada.Exceptions is part of the compiler sources.

--  The base version of this unit Ada.Exceptions omits the Wide version of
--  Exception_Name and is used to build the compiler and other basic tools.

pragma Polling (Off);
--  We must turn polling off for this unit, because otherwise we get
--  elaboration circularities with ourself.
with Interfaces;

package Ada.Exceptions is

   --  In accordance with Ada 2005 AI-362. The warnings pragmas are so that we
   --  can compile this using older compiler versions, which will ignore the
   --  pragma, which is fine for the bootstrap.

   type Exception_Occurrence is new Interfaces.Unsigned_32;

end Ada.Exceptions;
