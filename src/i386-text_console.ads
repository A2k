with I386.Port_IO;

package I386.Text_Console is

   procedure Put (C : Character);

   procedure Put (S : String);

   procedure Cls;

   procedure New_Line;

   procedure Put_Line (S : String);

private

   type Console_Index is new Unsigned_32;
   for Console_Index'Size use 32;

   Max_Line : constant  Console_Index := 25;
   Max_Column : constant Console_Index := 80;

   subtype Line_Index is Console_Index range 1 .. Max_Line;
   subtype Column_Index is Console_Index range 1 .. Max_Column;

   Video_Base_Address : constant I386.Port_IO.Port_Address := 16#B8000#;

   type Unsigned_4 is new Unsigned_8 range 0 .. 15;
   for Unsigned_4'Size use 4;

   type Text_Attribute is
      record
         F_Color : Unsigned_4;
         B_Color : Unsigned_4;
      end record;
   for Text_Attribute'Size use 8;
   pragma Pack (Text_Attribute);

   type Text_Element is
      record
         Text : Unsigned_8;
         Attribute : Text_Attribute := (15, 0);
      end record;
   for Text_Element'Size use 16;

   type Text_Line_Buffer is array (Column_Index) of Text_Element;

   type Text_Buffer is array (Line_Index) of  Text_Line_Buffer;
   for Text_Buffer'Size use 32000;

   Blank_Text_Element : constant Text_Element :=
     (Character'Pos (Ascii.NUL), (15, 0));

end I386.Text_Console;
