package I386.Port_IO is

   type Port_Address is new Unsigned_32;

   function Get (Port : Port_Address) return Unsigned_8;
   pragma Inline (Get);

   function Get (Port : Port_Address) return Unsigned_16;
   pragma Inline (Get);

   function Get (Port : Port_Address) return Unsigned_32;
   pragma Inline (Get);

   procedure Put (Port : Port_Address; Data : Unsigned_8);
   pragma Inline (Put);

   procedure Put (Port : Port_Address; Data : Unsigned_16);
   pragma Inline (Put);

   procedure Put (Port : Port_Address; Data : Unsigned_32);
   pragma Inline (Put);

end I386.Port_IO;
