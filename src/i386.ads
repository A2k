with Interfaces; use Interfaces;
with Ada.Unchecked_Conversion;
with System;

package I386 is

   pragma Pure;

   subtype Unsigned_8 is Interfaces.Unsigned_8;
   subtype Unsigned_16 is Interfaces.Unsigned_16;
   subtype Unsigned_32 is Interfaces.Unsigned_32;

   function To_Address is new Ada.Unchecked_Conversion
     (Unsigned_32, System.Address);

end I386;
