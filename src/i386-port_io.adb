with System.Machine_Code;

package body I386.Port_IO is

   function Get (Port : Port_Address) return Unsigned_8
   is
      use System.Machine_Code;
      Result : Unsigned_8 := 0;
   begin
      Asm ("inb %w1, %b0",
           Inputs  => Port_Address'Asm_Input ("d", Port),
           Outputs => Unsigned_8'Asm_Output ("=a", Result),
           Clobber => "",
           Volatile => True);
      return Result;
   end Get;

   function Get (Port : Port_Address) return Unsigned_16
   is
      use System.Machine_Code;
      Result : Unsigned_16 := 0;
   begin
      Asm ("inw %w1, %w0",
           Inputs  => Port_Address'Asm_Input ("d", Port),
           Outputs => Unsigned_16'Asm_Output ("=a", Result),
           Clobber => "",
           Volatile => True);
      return Result;
   end Get;

   function Get (Port : Port_Address) return Unsigned_32
   is
      use System.Machine_Code;
      Result : Unsigned_32 := 0;
   begin
      Asm ("inl %w1, %0",
           Inputs  => Port_Address'Asm_Input ("d", Port),
           Outputs => Unsigned_32'Asm_Output ("=a", Result),
           Clobber => "",
           Volatile => True);
      return Result;
   end Get;

   procedure Put (Port : Port_Address; Data : Unsigned_8)
   is
      use System.Machine_Code;
   begin
      Asm ("outb %b0, %w1",
           Inputs  => (Unsigned_8'Asm_Input ("a", Data),
                       Port_Address'Asm_Input ("d", Port)
                      ),
           Clobber => "",
           Volatile => True);
   end Put;

   procedure Put (Port : Port_Address; Data : Unsigned_16)
   is
      use System.Machine_Code;
   begin
      Asm ("outw %w0, %w1",
           Inputs  => (Unsigned_16'Asm_Input ("a", Data),
                       Port_Address'Asm_Input ("d", Port)
                      ),
           Clobber => "",
           Volatile => True);
   end Put;

   procedure Put (Port : Port_Address; Data : Unsigned_32)
   is
      use System.Machine_Code;
   begin
      Asm ("outl %0, %w1",
           Inputs  => (Unsigned_32'Asm_Input ("a", Data),
                       Port_Address'Asm_Input ("d", Port)
                      ),
           Clobber => "",
           Volatile => True);
   end Put;

end I386.Port_IO;
