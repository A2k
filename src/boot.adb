pragma No_Run_Time;

with I386.Text_Console;

procedure Boot is

begin
   I386.Text_Console.Cls;
   I386.Text_Console.Put_Line ("booting the kernel!");
   for I in 1 .. 30 loop
   for J in 1 .. 79 loop
      I386.Text_Console.Put ('.');
   end loop;
   I386.Text_Console.Put (Character'Val (I));
   end loop;
   I386.Text_Console.Put_Line ("scrooling console test: done");
end Boot;
