with System;

package Crtl is
   pragma Preelaborate;

   type size_t is mod 2 ** Standard'Address_Size;

   function  memcpy
     (S1 : System.Address; S2 : System.Address; N : size_t)
     return System.Address;
   pragma Export (C, memcpy, "memcpy");

end Crtl;
