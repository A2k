with Interfaces;
with Unchecked_Conversion;
with System.Storage_Elements;

package body Crtl is

   function memcpy
     (S1 : System.Address; S2 : System.Address; N : size_t)
     return System.Address
   is
      type Pointer is access all Interfaces.Unsigned_8;

      function To_Pointer is new Unchecked_Conversion (System.Address, Pointer);
      function To_Address is new Unchecked_Conversion (Pointer, System.Address);

      ---------
      -- "+" --
      ---------

      function "+" (Left : Pointer; Right : size_t) return Pointer is
         use System.Storage_Elements;
      begin
         return To_Pointer (To_Address (Left) +
                              System.Storage_Elements.Storage_Offset (Right));
      end "+";
      pragma Inline ("+");

      ---------------
      -- Increment --
      ---------------

      procedure Increment (Ref : in out Pointer) is
      begin
         Ref := Ref + 1;
      end Increment;
      pragma Inline (Increment);

      T : Pointer := To_Pointer (S1);
      S : Pointer := To_Pointer (S2);

   begin
      --        if S = null or else T = null then
      --           raise Dereference_Error;
      --        else
      for J in 1 .. N loop
         T.all := S.all;
         Increment (T);
         Increment (S);
      end loop;
      --       end if;
      return S1;
   end memcpy;

end Crtl;
