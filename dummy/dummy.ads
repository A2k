pragma No_Run_Time;

package Dummy is
   pragma Preelaborate;

   procedure Eh_Personality;
   pragma Export (C, Eh_Personality, "__gnat_eh_personality");

end Dummy;
